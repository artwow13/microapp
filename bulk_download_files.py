import requests
 
def download_url(url): #создаём функцию - это мини-програмка выполняющая действие
  print("downloading: ",url) #напечатаем Скачиваем и дальше ссылка на файл
  # Важный момент - мы считаем что после слеша идёт название файла, что нам пригодится при сохранении его
  # Если url выглядит так abc/xyz/file.txt, то итоговое имя файла будет таким file.txt
  file_name_start_pos = url.rfind("/") + 1
  file_name = url[file_name_start_pos:] #Задаём как будет выгядеть файлнейм
 
  r = requests.get(url, stream=True)
  if r.status_code == requests.codes.ok:
    with open(file_name, 'wb') as f:
      for data in r:
        f.write(data)
 
#Дальше мы массово скачиваем по каждой ссылке 1 раз
download_url("https://jsonplaceholder.typicode.com/posts")
download_url("https://jsonplaceholder.typicode.com/comments")
download_url("https://jsonplaceholder.typicode.com/photos")
download_url("https://jsonplaceholder.typicode.com/todos")
download_url("https://jsonplaceholder.typicode.com/albums")
